from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_basicauth import BasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
import sys, os

# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()
b_auth = BasicAuth()

#def create_app(name, path):
def create_app():

    app=Flask(__name__, static_url_path='')

    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///lora.sqlite'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
    app.config['TEMPLATES_AUTO_RELOAD'] = True


    db.init_app(app)

    from models import User, Lora


    # blueprint for non-auth parts of app
    from main import main as main_blueprint
    app.register_blueprint(main_blueprint)
    return app

