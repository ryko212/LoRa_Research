"""Flask app configuration."""
from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

class Config:
    """Set Flask configuration from environment variables."""

    FLASK_ENV = environ.get('FLASK_ENV')
    SECRET_KEY = environ.get('SECRET_KEY')

    SQLALCHEMY_DATABASE_URI = 'sqlite:///lora.sqlite'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Basic Auth
    BASIC_AUTH_FORCE=environ.get('BASIC_AUTH_FORCE')
    BASIC_AUTH_REALM=environ.get('Login Required')
    BASIC_AUTH_USERNAME=environ.get('BASIC_AUTH_USERNAME')
    BASIC_AUTH_PASSWORD=environ.get('BASIC_AUTH_PASSWORD')

    # Flask-Assets
    LESS_BIN = environ.get('LESS_BIN')
    ASSETS_DEBUG = environ.get('ASSETS_DEBUG')
    LESS_RUN_IN_DEBUG = environ.get('LESS_RUN_IN_DEBUG')

    # Static Assets
    STATIC_FOLDER = 'static'
    TEMPLATES_FOLDER = 'templates'
    COMPRESSOR_DEBUG = environ.get('COMPRESSOR_DEBUG')

    # Flask-SQLAlchemy
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
