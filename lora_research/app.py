from flask import Flask
from flask_socketio import SocketIO, emit
import config
from __init__ import *

app = create_app()

app.config.from_object('config.Config')

#socketio = SocketIO(app, async_mode=None, logger=True, engineio_logger=True)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
    #socketio.run(app)
