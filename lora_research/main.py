from flask import Response, Blueprint, render_template, request, flash, abort, url_for
from flask_login import login_required, current_user
from __init__ import db, b_auth
from models import User, Lora, Sensor
from flask_socketio import SocketIO, emit
from flask_sqlalchemy import Pagination
import os
import time
import datetime
import json

main = Blueprint('main', __name__)

mac_check = []
with open('static/mac.json') as f:
    for line in f:
        mac_check.append(json.loads(line))
mac_check = sorted(mac_check, key=lambda x: len(x['oui']), reverse=True)


@main.route('/post_lora', methods=['POST'])
def proc_lora():
    allowed = [x.sensor for x in Sensor.query.all()]
    lora_data = request.get_json()
    sensor_id = lora_data['id']
    mac_id = lora_data['mac']
    if sensor_id in allowed:
        sens_id = Sensor.query.filter_by(sensor=sensor_id).first()
        try:
            lora_id = Lora.query.filter_by(mac=mac_id).first()
            temp_count = lora_id.count + 1
            update_entry = {
                'mac':lora_id.mac,
                'seen':datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'count':temp_count,
                'oui':lora_id.oui,
                'sens_id':sens_id.id}
            temp = Lora.query.filter_by(id=lora_id.id).update(update_entry)
        except Exception as e:
            ### new entry ###
            mac_entry = Lora(
                mac=mac_id,
                seen=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                count = 1,
                oui = find_oui(mac_id),
                sens_id=sens_id.id)
            db.session.add(mac_entry)
        db.session.commit()
        #return render_template('index.html')
        status_code = Response(status=200) 
        return status_code
    else:
        abort(404)

def find_oui(mac):
    temp_mac = ":".join([mac.upper()[i:i+2] for i in range(0, len(mac), 2)])
    for entry in mac_check:
        if entry['oui'] in temp_mac[:13]:
            return entry['companyName']
    return "OUI Not Found"

@main.route('/')
@main.route('/index')
def index():
    page = request.args.get('page', 1, type=int)
    temp_loras = Lora.query.order_by(Lora.count.desc()).paginate(page, per_page=15)
    loras = Lora.query.order_by(Lora.count.desc()).paginate(page, per_page=15).items

    lora_table = []
    for lora in loras:
        temp_sens = Sensor.query.filter_by(id=lora.sens_id).first()
        lora_table.append({
            "id":lora.id,
            "mac":':'.join([lora.mac[i:i+2] for i in range(0, len(lora.mac), 2)]),
            "seen":lora.seen,
            "count":lora.count,
            "lat": str(temp_sens.lat),
            "lon": str(temp_sens.lon),
            "sens_id": temp_sens.sensor,
            "oui": lora.oui})
    lora_table = sorted(lora_table, key=lambda x: x['count'], reverse=True)
    
    next_url = url_for('main.index', page=temp_loras.next_num) if temp_loras.has_next else None
    prev_url = url_for('main.index', page=temp_loras.prev_num) if temp_loras.has_prev else None

    return render_template('index.html', table=lora_table, temp_loras=temp_loras, next_url=next_url, prev_url=prev_url, page=page, per_page=15) 


@main.route('/full_list')
def full_list():
    loras = Lora.query.all()
    lora_table = []
    for lora in loras:
        temp_sens = Sensor.query.filter_by(id=lora.sens_id).first()
        lora_table.append({
            "id":lora.id,
            "mac":':'.join([lora.mac[i:i+2] for i in range(0, len(lora.mac), 2)]),
            "seen":lora.seen,
            "count":lora.count,
            "lat": str(temp_sens.lat),
            "lon": str(temp_sens.lon),
            "sens_id": temp_sens.sensor,
            "oui": lora.oui})
    lora_table = sorted(lora_table, key=lambda x: x['count'], reverse=True)
    return render_template('full_list.html', table=lora_table)


@main.route('/sensors')
@b_auth.required
def sensors():
    sensors = Sensor.query.all()
    sensor_table = []
    for sensor in sensors:
        sensor_table.append({
            "id":sensor.id, 
            "sensor":sensor.sensor, 
            "lat": sensor.lat,
            "lon": sensor.lon})
    sensor_table = sorted(sensor_table, key=lambda x: x['sensor'])
    return render_template('sensors.html', table=sensor_table)


@main.route('/sensors', methods=['POST'])
def get_sensor_update():
    temp_id = request.form.get('sensor_id')
    if 'get' in request.form:
        entry = request.form.get('sensor_id')
        temp_id = request.form.get('sensor_id')
        sensor = Sensor.query.get(entry)
        sensor_table = get_sensor_dropdown(temp_id) 
        return render_template('sensors.html', id=entry, sensor=sensor.sensor, lat=sensor.lat, lon=sensor.lon, table=sensor_table)
    elif 'update' in request.form:
        update_entry = {
            'id':request.form.get('sensor_id'), 
            'sensor':request.form.get('sensor'),
            'lat':request.form.get('lat'), 
            'lon':request.form.get('lon')} 
        temp = Sensor.query.filter_by(id=temp_id).update(update_entry)
        db.session.commit()
        sensor = Sensor.query.get(temp_id)
        sensor_table = get_sensor_dropdown(temp_id)
        flash('Current sensor updated in the database.', 'error')
        return render_template('sensors.html', id=sensor.id, sensor=sensor.sensor, lat=sensor.lat, lon=sensor.lon, table=sensor_table)
    elif 'new' in request.form:
        sensor = Sensor(
            sensor=request.form.get('sensor'), 
            lat=request.form.get('lat'), 
            lon=request.form.get('lon')) 
        db.session.add(sensor)
        db.session.commit()

        ### will need to be refactored - may need to relook get_beer_dropdown
        sensors = Sensor.query.all()
        sensor_table = []
        for sensor in sensors:
            sensor_table.append({
                "id":sensor.id,
                "sensor":sensor.sensor,
                "lat": sensor.lat,
                "lon": sensor.lon})
        sensor_table = sorted(sensor_table, key=lambda x: x['sensor'])
        flash('New sensor added to the database.', 'error')
        return render_template('sensors.html', table=sensor_table)

def get_sensor_dropdown(idx):
    sensors = Sensor.query.all()
    sensor_table = []
    for s in sensors:
        sensor_table.append({
            "id":s.id, 
            "sensor":s.sensor, 
            "lat": s.lat,
            "lon": s.lon})
    init = Sensor.query.get(idx)
    temp_list = [{
            "id":init.id, 
            "sensor":init.sensor, 
            "lat": init.lat,
            "lon": init.lon}]
    sensor_table.remove(temp_list[0])
    sensor_table = temp_list + sorted(sensor_table, key=lambda x: x['sensor'])
    return sensor_table


@main.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "-1"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

@main.route('/profile')
@login_required
def profile():
    return render_template('profile.html', name=current_user.name)
