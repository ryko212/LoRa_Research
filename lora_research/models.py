# models.py

from flask_login import UserMixin
from __init__ import db

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))

class Lora(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mac = db.Column(db.String(17))
    seen = db.Column(db.String(20))
    count = db.Column(db.Integer)
    oui = db.Column(db.String(50))
    sens_id = db.Column(db.Integer, db.ForeignKey('sensor.id'), nullable=False)
    sens = db.relationship('Sensor', backref='lora')

class Sensor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sensor = db.Column(db.String(12), unique=True, nullable=False)
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)


