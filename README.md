# Lora_Research

### Installation

Assuming an Ubuntu Server 18.04 base install:

#### Add users
```bash
adduser user
usermod -aG sudo user
adduser a
usermod -aG www-data a
```

#### Set up firewall
```bash
ufw allow OpenSSH
ufw allow "Nginx Full"
ufw enable
```

#### Add keys to authorized_keys

#### Add private keys (correct permissions)

#### Switch to user
```bash
exit
ssh user@X.X.X.X
```

#### set up environment
```bash
sudo apt-get install git python3-virtualenv
virtualenv -p python3 lora
cd lora
. bin/activate
git clone git@gitlab.com:ryko212/LoRa_Research.git
sudo apt-get install $(grep -vE "^\s*#" ~/lora/LoRa_Research/lora_research/support/apt_installs.txt  | tr "\n" " ")
pip3 install -r ~/lora/LoRa_Research/requirements.txt
```

### set up configuration file
```bash
cd ~/lora/LoRa_Research/lora_research/support
cp nginx_config.example nginx_config.txt
vim nginx_config.txt # plug in your info as needed
cp app.ini.example app.ini
vim app.ini # plug in your info as needed (log location, etc)
```

### set up cwnf service
```bash
cp lora.service.bak lora.service
vim lora.service # set up your paths right and user/group info
sudo cp lora.service /etc/systemd/system/
systemctl enable lora
```

### set up nginx and uwsgi
```bash
sudo service nginx stop # so letsencrypt doesn't cry about not being able to bind to the port
sudo letsencrypt certonly -d yourhostname.something.nothing
cd .. # lets keep certs/keys out of git path because sekyoooritee
mkdir ssl
sudo cp /etc/letsencrypt/live/yourhostname.yoursite/* ssl/
vim ~/lora/LoRa_Research/lora_research/support/nginx_config.conf # adjust to suit for ssl path you chose and hostname
sudo cp nginx_config.conf /etc/nginx/sites-available/cwnf.conf
sudo ln -s /etc/nginx/sites-available/cwnf.conf /etc/nginx/sites-enabled/
sudo nginx -t # should be no errors
sudo service lora start # fire up the lora service (creates the socket needed for nginx)
sudo service nginx start
```

### make the .env file in the lora_research directory with the following content
```bash
SECRET_KEY= 'your_db_secret_key'
BASIC_AUTH_USERNAME='put_your_username_here'
BASIC_AUTH_PASSWORD='put_your_password_here'
BASIC_AUTH_FORCE=True
BASIC_AUTH_REALM='Login Required'
```
